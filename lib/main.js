//run main function in start
$(document).ready(function(){
  main();
});


function main (){
  var board_config = {
    draggable : true,
    position: 'start',
    onDrop:sendMove
  };
  board = new ChessBoard('board', board_config);
  serv = new server({freqMax: 19000, freqMin: 17000});
  function sendMove (source, target) {
    if(source !== target){
      serv.send(source + "-" + target);
    }
  }
}
var cl = new client({freqMax: 19000, freqMin: 17000});
cl.start();
