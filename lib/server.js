var board, serv; //These will be defined in main()
var audioContext = window.audioContext || new webkitAudioContext();
/**
 * A simple sonic encoder/decoder for [a-z0-9] => frequency (and back).
 * A way of representing characters with frequency.
 */
var ALPHABET = '\n abcdefgh-12345678';

function server (params) {
  params = params || {};
  this.freqMin = params.freqMin || 17500;
  this.freqMax = params.freqMax || 19500;
  this.freqError = params.freqError || 50;
  this.alphabetString = params.alphabet || ALPHABET;
  this.startChar = params.startChar || '^';
  this.endChar = params.endChar || '$';
  this.alphabet = this.startChar + this.alphabetString + this.endChar;
  this.charDuration = 0.1;
}

server.prototype.str_to_char = function(string){
  stringArray = string.split('');
  for (var achar in stringArray) {
    console.log(stringArray[achar]);
  }
};

server.prototype.send = function(msg) {
  msg = this.startChar + msg + this.endChar + this.endChar + this.endChar;
  console.log(msg);
  var osc = audioContext.createOscillator();
  osc.connect(audioContext.destination);
  osc.start(0);
  for (var i = 0; i < msg.length; i++) {
    var chr = msg[i];
    var freq = this.char_to_freq(chr);
    var t = audioContext.currentTime + this.charDuration * i;
    osc.frequency.setValueAtTime(freq, t);
  }
  var stopt = audioContext.currentTime + this.charDuration * msg.length;
  osc.stop(stopt);
};

server.prototype.char_to_freq = function(char){
  // Get the index of the character.
  index = this.alphabet.indexOf(char);
  if (index != -1){
    var freqRange = this.freqMax - this.freqMin;
    var percent = index / this.alphabet.length;
    var freqOffset = Math.round(freqRange * percent);
    return this.freqMin + freqOffset;
  }
  // If this character isn't in the alphabet, error out.
  console.error(char, 'is an invalid character.');
  return false;
};

module.exports = server;
