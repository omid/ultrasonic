var ALPHABET = '\n abcdefgh-12345678';

function client (params) {
  params = params || {};
  this.freqMin = params.freqMin || 17500;
  this.freqMax = params.freqMax || 19500;
  this.freqError = params.freqError || 50;
  this.alphabetString = params.alphabet || ALPHABET;
  this.startChar = params.startChar || '^';
  this.endChar = params.endChar || '$';
  this.alphabet = this.startChar + this.alphabetString + this.endChar;
  this.peakThreshold = params.peakThreshold || -65;
  this.tmpdata = '';
}

client.prototype.validate_move = function(move) {
  var regex = new RegExp('[a-h][1-8]-[a-h][1-8]');
  if(regex.test(move)){
    return move;
  }
  return false;
};

client.prototype.onRecieve = function(msg) {
  if(this.validate_move(msg) !== false){
    board.move(msg);
  }
};

client.prototype.start = function() {
  navigator.webkitGetUserMedia({audio:true}, this.onStream_.bind(this));
};
client.prototype.onStream_ = function(stream) {
  this.stream = stream;
  var input = audioContext.createMediaStreamSource(stream);
  var analyser = audioContext.createAnalyser();
  input.connect(analyser);
  this.freqs = new Float32Array(analyser.frequencyBinCount);
  this.analyser = analyser;
  this.isRunning = true;
  // The following function will be executed on each frame.
  requestAnimationFrame(this.loop.bind(this));
};

client.prototype.loop = function() {
  this.analyser.getFloatFrequencyData(this.freqs);
  var freq = this.getPeakFrequency();
  if(this.freq_to_char(freq) != undefined){
    this.tmpdata += this.freq_to_char(freq);
  }
  if (this.freq_to_char(freq) == "$") {
    this.tmpdata = this.tmpdata.replace(/([a-h1-8^$-])\1+/g,"\$1");
    this.onRecieve(this.tmpdata.slice(1,-1));
    console.log(this.tmpdata);
    this.tmpdata = '';
  }
  if (this.isRunning) {
    requestAnimationFrame(this.loop.bind(this));
  }
};

client.prototype.getPeakFrequency = function() {
  // Find where to start.
  var start = this.freqToIndex(this.freqMin);
  // TODO: use first derivative to find the peaks, and then find the largest peak.
  // Just do a max over the set.
  var max = -Infinity;
  var index = -1;
  for (var i = start; i < this.freqs.length; i++) {
    if (this.freqs[i] > max) {
      max = this.freqs[i];
      index = i;
    }
  }
  // Only care about sufficiently tall peaks.
  // console.log(max); //DEBUG
  if (max > this.peakThreshold) {
    return this.indexToFreq(index);
  }
  return null;
};

client.prototype.freq_to_char = function(freq){
  // If the frequency is out of the range.
  if (!(this.freqMin < freq && freq < this.freqMax)) {
    // If it's close enough to the min, clamp it (and same for max).
    if (this.freqMin - freq < this.freqError) {
      freq = this.freqMin;
    } else if (freq - this.freqMax < this.freqError) {
      freq = this.freqMax;
    } else {
      // Otherwise, report error.
      console.error(freq, 'is out of range.');
      return null;
    }
  }
  // Convert frequency to index to char.
  var freqRange = this.freqMax - this.freqMin;
  var percent = (freq - this.freqMin) / freqRange;
  var index = Math.round(this.alphabet.length * percent);
  return this.alphabet[index];
};

client.prototype.indexToFreq = function(index) {
  var nyquist = audioContext.sampleRate/2;
  return nyquist/this.freqs.length * index;
};

client.prototype.freqToIndex = function(frequency) {
  var nyquist = audioContext.sampleRate/2;
  return Math.round(frequency/nyquist * this.freqs.length);
};
module.exports = client;
