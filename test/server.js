var server = require("./../lib/server");
var assert = require("assert");

describe("server", function() {
  describe("#char_to_freq()", function(){
    it("should return false for invalid (out of our ALPHABET) Charachters", function(){
      var se = new server();
      assert.equal(false, se.char_to_freq('z'));
      assert.equal(false, se.char_to_freq('-'));
      assert.equal(false, se.char_to_freq('9'));
    });
    it("should return frequency for valid char", function(){
      var se = new server();
      assert.equal(18600, se.char_to_freq('a'));
      assert.equal(19000, se.char_to_freq('1'));
    });
  });
});
