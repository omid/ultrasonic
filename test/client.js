var client = require("./../lib/client");
var assert = require("assert");

describe("client", function() {
  describe("#validate_move()", function(){
    it("should return false for invalid (out of board) moves.", function(){
      var cl = new client();
      assert.equal(false, cl.validate_move('a0-b2'));
      assert.equal(false, cl.validate_move('a8-b9'));
      assert.equal(false, cl.validate_move('o1-b5'));
    });
    it("should return true for valid moves.", function(){
      var cl = new client();
      assert.equal('a1-b2', cl.validate_move('a1-b2'));
      assert.equal('a1-a3', cl.validate_move('a1-a3'));
      assert.equal('a1-b3', cl.validate_move('a1-b3'));
    });
  });
});
